#-------------------------------------------------
#
# Project created by QtCreator 2014-10-03T09:43:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stega
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    steganography.cpp

HEADERS  += mainwindow.h \
    steganography.h

FORMS    += mainwindow.ui
