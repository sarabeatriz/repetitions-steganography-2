/// \file

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>
#include <QLabel>
#include "steganography.h"


StegaPanel::StegaPanel(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StegaPanel)
{
    ui->setupUi(this); //

}

void StegaPanel::hideMessage()
{

    string binMsg ;         // Will be used to store the message in a binary String format

    ui->error_msg_label->clear(); //clear error text
    orig_msg = ui->textEdit->toPlainText(); //obtain msg from textEdit

    if(old_image.isNull()){
        qDebug() << "Load an image first!";
        ui->error_msg_label->setText("NO IMAGE LOADED!");
    }
    else if (orig_msg == NULL){
        ui->error_msg_label->setText("No message to hide!");
    }
    else{

        binMsg = messageToBinaryString(orig_msg.toStdString()) ;

        if(((old_image.width() * old_image.height()) * 3) < (int)binMsg.length())
        {
            ui->error_msg_label->setText("IMAGE NOT BIG ENOUGH FOR MESSAGE!");
        }
        else
        {
            EmbbedMessage(old_image, new_image, orig_msg.toStdString()) ;
        }

        ui->error_msg_label->setText("Message hidden in Stego Image with 1 stego bits.");
    }
}


void StegaPanel::revealMessage()
{

    string msg = ExtractMessage(new_image) ;

    if (msg.length() > 0){
        ui->textEdit->setText(QString::fromStdString(msg));
        ui->error_msg_label->setText("Message extracted!");
    }
    else{
        ui->error_msg_label->setText("Could not extract message!");
    }
}

void StegaPanel::on_loadImage_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this, tr("Choose an image"), QDir::homePath());
        if (!fname.isEmpty()){
            QImage image(fname);
            if (image.isNull())
                QMessageBox::information(this, tr("Choose an image"),tr("Cannot load %1.").arg(fname));
            old_image=image;
            new_image=image;
            repaint();
        }
        ui->old_image->setPixmap(QPixmap::fromImage(old_image));
        ui->new_image->setPixmap(QPixmap::fromImage(new_image));
        ui->textEdit->setText("");
        ui->error_msg_label->setText("Image loaded!");
}

void StegaPanel::on_hideMessage_clicked()
{
    hideMessage();
    ui->new_image->setPixmap(QPixmap::fromImage(new_image));
}

void StegaPanel::on_getMessage_clicked()
{
    revealMessage();
}

StegaPanel::~StegaPanel()
{
    delete ui;
}

void StegaPanel::on_storeImage_clicked()
{
    QPixmap out = QPixmap::grabWidget(this,361,10,481,481);
    QString fname = QFileDialog::getSaveFileName(this, tr("Save Edited Image"), (""), tr("PNG (*.png)" ));
    new_image.save(fname, "PNG");
    ui->error_msg_label->setText("Image saved to" + fname + " !");
}
